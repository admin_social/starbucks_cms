<?php

require_once 'privado/config.php';
require_once 'privado/conecta_db.php';
require_once 'privado/OperacionesCafes.php';
require_once 'privado/GenerarXML.php';

$respuesta = array('success' => false, 'msg' => '');
$ac = isset($_REQUEST['accion']) ? $_REQUEST['accion'] : '';

$bd_link = conecta_db();
GenerarXML::xml_cafes($bd_link);
$respuesta['success'] = true;
print json_encode($respuesta);
?>
