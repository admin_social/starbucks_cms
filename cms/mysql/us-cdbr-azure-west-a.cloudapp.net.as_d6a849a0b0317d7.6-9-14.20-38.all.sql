# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: us-cdbr-azure-west-a.cloudapp.net (MySQL 5.5.21-log)
# Database: as_d6a849a0b0317d7
# Generation Time: 2014-06-10 01:38:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tabebidasmedidas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabebidasmedidas`;

CREATE TABLE `tabebidasmedidas` (
  `FIIDBEBIDA` int(11) NOT NULL,
  `FIIDMEDIDA` int(11) NOT NULL,
  PRIMARY KEY (`FIIDBEBIDA`,`FIIDMEDIDA`),
  KEY `FIIDMEDIDA` (`FIIDMEDIDA`),
  CONSTRAINT `tabebidasmedidas_ibfk_1` FOREIGN KEY (`FIIDBEBIDA`) REFERENCES `tabebidas` (`FIIDPRODUCTO`) ON DELETE CASCADE,
  CONSTRAINT `tabebidasmedidas_ibfk_2` FOREIGN KEY (`FIIDMEDIDA`) REFERENCES `tamedidas` (`FIIDMEDIDA`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
