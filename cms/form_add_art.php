<?php
require_once 'privado/comprobar_sesion.php';
require_once 'privado/config.php';
//require_once 'privado/Operaciones.php';
//require_once 'privado/OperacionesCards.php';
require_once 'privado/conecta_db.php';
require_once 'includes/cabecera.php';

$bd_link = conecta_db();
?>
<script>


$(document).ready(function(e){
  //alert("Listo");

  var all_valid=true;

  $('#bin_inicial').on('blur',function(e){
    dpat= /^[0-9]{16}$/;
    if(dpat.test(this.value)){
      all_valid=true;
    }else{
      all_valid=false;
      alert("BIN Inicial incorrecto, solo debe contener 16 digitos");
    }
  });

  $('#bin_final').on('blur',function(e){
    dpat= /^[0-9]{16}$/;
    if(dpat.test(this.value)){
      all_valid=true;
    }else{
      all_valid=false;
      alert("BIN final incorrecto, solo debe contener 16 digitos");
    }
  });

  $('#uploadimage').on('submit',(function(e) {
    e.preventDefault();
    if(all_valid){
    $('#loader_guardando').show();

    $.ajax({
      url:'cards.php',
      type:"POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData:false,

      success: function(data){
        $('#loader_guardando').hide();
        $('#guardar').value="GUARDADO!";
        $('#message').html(data);
      }
    });
    }
  }));
  
});
</script>

<style>
    #sortable { list-style-type: none; margin: 0; padding: 0; }
    #sortable li { margin: 3px 3px 3px 3px; padding: 1px 1px 1px 1px; height: 76px;float: left; text-align: center; border: none }
</style>

<div class="holder">

  <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
    <table style="border: 1px solid #D3D3D3;width: 98%;margin: 10px;padding: 20px">
        <tr>
          <td colspan="2" style="text-align: center;font-weight: bold;font-size: 1.3em;padding-bottom: 30px"><span class="titulo_formulario">Formulario de alta de arte de tarjetas</span></td>
        </tr>
        <tr>
          <td class="etiqueta_campo">BIN Inicial: <span class="color_campo_obligatorio">*</span></td>
          <td><input id="bin_inicial" name="bin_inicial" type="text"></td>
        </tr>

        <tr>
          <td class="etiqueta_campo">BIN Final: <span class="color_campo_obligatorio">*</span></td>
          <td><input id="bin_final" name="bin_final" type="text"></td>
        </tr>
        <tr>
          <td class="etiqueta_campo">Imagen principal <span class="color_campo_obligatorio">*</span></td>
          <td>
            <table style="padding: 0px;margin: 0px" cellpadding="0" cellspacing="0">
              <tr>
                <td><input id="input_imagen_principal" type="file" name="imagen" /></td>
                <td id="contenedor-imagen-principal"> </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2" class="color_campo_obligatorio" style="text-align: right">* Obligatorio</td>
        </tr>
        <tr>
          <td></td>
          <td><br/> <input id="guardar" type="submit" value="Guardar" name="submit">
<img id="loader_guardando" src="gfx/images/ajax-loader.gif" style="display: none" />
</td>
        </tr>

  </table>
<div class="message" id="message"></div>
</div>

<?php require_once 'includes/pie.php'; ?>
