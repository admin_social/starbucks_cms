<?php
//error_reporting(E_ALL);
//include('../privado/config.php');
require_once '../privado/config.php';
require_once '../privado/conecta_db.php';
require_once '../privado/OperacionesCards.php';
header("Content-Type: application/json");

if(isset($_GET['BIN'])){
  $bin=$_GET['BIN'];
}else{
  $bin='abc';
}
//$domain="https://cms-rewards.starbucks.mx/cms";

class CardAnswer{
  /*private $domain="http://testcms-rewards-starbucks.azurewebsites.net/cms/cards/art/";*/
  private $domain="https://cms-rewards.starbucks.mx/cms/cards/art/";
  public $bin="";

  public $imagen = array( 
     "hd" => 'http://testcms-rewards-starbucks.azurewebsites.net/cms/cards/art/default.png',
     "mediana" => 'http://testcms-rewards-starbucks.azurewebsites.net/cms/cards/art/default_165.png',
     "grande" => 'http://testcms-rewards-starbucks.azurewebsites.net/cms/cards/art/default_261.png',
     "icon" => 'http://testcms-rewards-starbucks.azurewebsites.net/cms/cards/art/default_88.png',
     "chica" => 'http://testcms-rewards-starbucks.azurewebsites.net/cms/cards/art/default_115.png');

  function __construct($bin, $icon, $chica, $mediana, $grande, $hd){
    $this->bin=$bin;
    //$url=end($url);
    $this->imagen["icon"]=$this->domain.$icon;
    $this->imagen["chica"]=$this->domain.$chica;
    $this->imagen["mediana"]=$this->domain.$mediana;
    $this->imagen["grande"]=$this->domain.$grande;
    $this->imagen["hd"]=$this->domain.$hd;
  }
  
}

function validateBIN($bin_num){
  $icon   = "default_88.png";
  $chica  = "default_115.png";
  $mediana= "default_165.png";
  $grande = "default_261.png";
  $hd     = "default.png";
  //check if its number
  if(is_numeric($bin_num)){
    //$card = new CardAnswer($bin_num,"tarjeta_".strval($bin_num).".png");
    $bd_link = conecta_db();
    $card_url=OperacionesCards::obtiene_card($bd_link, $bin_num);
    //print_r($card_url);
    //print(empty($card_url));
    
    if(empty($card_url)!=1){
      $card_url=end($card_url);
      $mediana=($card_url['fcartesmall']);
      $grande=($card_url['fcartelarge']);
      $chica=($card_url['fcartelittle']);
      $icon = ($card_url['fcarteicon']);
      $hd = $card_url['fcdiseno'].'.'.$card_url['fcimgtipo'];
    }
  }else{
    //$respuesta="Invalido";
    $bin_num="0000000000000000";
  }

  $card = new CardAnswer($bin_num,$icon,$chica, $mediana, $grande, $hd);
  return $card;

}


echo json_encode(validateBIN($bin));

?>
