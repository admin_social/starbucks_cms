<?php

class OperacionesCards {
    public static function insertar_card(PDO $bd_link,$nombre, $tipoimg, $bin_ini, $bin_fin, $card_small,$card_large, $card_little, $card_icon) {

      $sql = "INSERT INTO tacards (FCDISENO,FCIMGTIPO, FIBININICIAL,FIBINFINAL,FCARTESMALL, FCARTELARGE, FCARTELITTLE, FCARTEICON) 
        VALUES (:nombre,:tipoimg,:binini,:binfin,:artesmall,:artelarge, :artelittle, :arteicon)";
      $psql= $bd_link->prepare($sql);
      $psql->bindParam(':nombre',$nombre);
      $psql->bindParam(':tipoimg',$tipoimg);
      $psql->bindParam(':binini',$bin_ini);
      $psql->bindParam(':binfin',$bin_fin);
      $psql->bindParam(':artesmall',$card_small);
      $psql->bindParam(':artelarge',$card_large);
      $psql->bindParam(':artelittle',$card_little);
      $psql->bindParam(':arteicon',$card_icon);
/*        $sql.= $bin_ini . ",";
        $sql.= $bin_fin . ",";
        $sql.= $url;
        $sql.= ");";*/
        #$psql->debugDumpParams();
        if ($psql->execute() === FALSE) {
            $mensaje_error = $bd_link->errorInfo();
            $mensaje_error = print_r($mensaje_error);
            throw new Exception($mensaje_error);
        }
    }
    
    public static function obtiene_card(PDO $bd_link, $bin) {
      $sql ="select fcdiseno, fcimgtipo, fcartesmall,fcartelarge,fcartelittle, fcarteicon from tacards where :bin between fibininicial and fibinfinal";
      //$sql = "select fcartesmall, fcartelarge from tacards where :bin between fibininicial and fibinfinal";
      $psql = $bd_link->prepare($sql);
      $psql->bindParam(':bin',$bin);
      $result = $psql->execute();
      if ($result === FALSE) {
        $mensaje_error = $bd_link->errorInfo();
        $mensaje_error = $mensaje_error[2];
        throw new Exception($mensaje_error);
      }
        return $psql->fetchAll(PDO::FETCH_ASSOC);
    }


}

?>
