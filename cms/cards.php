<?php
require_once 'privado/comprobar_sesion.php';
require_once 'privado/config.php';
require_once 'privado/conecta_db.php';
require_once 'privado/OperacionesCards.php';
require_once 'includes/upload.php';

$bin_inicial =$_POST["bin_inicial"];
$bin_final = $_POST["bin_final"];
$target_dir ="cards/art/";

$bd_link = conecta_db();
  
if(isset($_FILES["imagen"]["type"])){
  $validextensions = array("jpeg","jpg","png");
  $temporary = explode(".",$_FILES["imagen"]["name"]);
  
  $filename = $temporary[0];
  $file_extension = end($temporary);

  $arte_s = $filename.'_165.'.$file_extension;
  $arte_l = $filename.'_261.'.$file_extension;
  $arte_m = $filename.'_115.'.$file_extension;
  $arte_i = $filename.'_88.'.$file_extension;
  
  $target_file_s = $target_dir . $arte_s;
  $target_file_l = $target_dir . $arte_l;
  if(
      ( ($_FILES["imagen"]["type"]=="image/png")||
        ($_FILES["imagen"]["type"]=="image/jpg")||
        ($_FILES["imagen"]["type"]=="image/jpeg")) &&
        ($_FILES["imagen"]["size"]<10000000) &&
        in_array($file_extension,$validextensions) 
  ){
    if($_FILES["imagen"]["error"]>0){
      echo "Error: " . $_FILES["imagen"]["error"]."<br/><br/>";
    }else{
        $sourcepath= $_FILES["imagen"]["tmp_name"];
        $bd_link->beginTransaction();
        $clean_target_s=addslashes($arte_s);
        $clean_target_l=addslashes($arte_l);
        OperacionesCards::insertar_card($bd_link,$filename, $file_extension, $bin_inicial,$bin_final,$arte_s, $arte_l, $arte_m, $arte_i);
        $bd_link->commit();
        
        $image_i = new Upload($_FILES["imagen"]);
        $image_i->resize=true;
        $image_i->image_x=88;
        $image_i->image_ratio_y=true;
        $image_i->file_new_name_body=$arte_i;
        $image_i->file_new_name_ext="";
        $image_i->Process($target_dir);

        $image_lt = new Upload($_FILES["imagen"]);
        $image_lt->resize=true;
        $image_lt->image_x=115;
        $image_lt->image_ratio_y=true;
        $image_lt->file_new_name_body=$arte_m;
        $image_lt->file_new_name_ext="";
        $image_lt->Process($target_dir);
        
        $image_s = new Upload($_FILES["imagen"]);
        $image_s->resize=true;
        $image_s->image_x=165;
        $image_s->image_ratio_y=true;
        $image_s->file_new_name_body=$arte_s;
        $image_s->file_new_name_ext="";
        $image_s->Process($target_dir);

        $image_l = new Upload($_FILES["imagen"]);
        $image_l->resize=true;
        $image_l->image_x=261;
        $image_l->image_ratio_y=true;
        $image_l->file_new_name_body=$arte_l;
        $image_l->file_new_name_ext="";
        $image_l->Process($target_dir);
        echo $image_l->file_new_name_body;
        //move_uploaded_file($sourcepath,$target_file);
        //guardar en la base de datos
        if($image_s->processed && $image_l->processed){
          echo "<span id='success'>Imagen agregada exitosamente!!!</span><br/>";
        }else{
          echo "<span id='invalid'>".$image_s->error." ".$image_l->error."</span><br/>";
        }
      }
  
  }else{
        echo "<span id='invalid'>Tama&ntilde;o o tipo inv&aacute;lido </span><br/>";
  }
}

?>
